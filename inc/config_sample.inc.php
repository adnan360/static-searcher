<?php

$config = array(
	0 => array(
		'type' => 'hexo',
		// Path to files to search. Without trailing slash at the end.
		'path' => dirname(dirname(__FILE__)) . '/data/example.gitlab.io',
		// or literal path:
		// 'path' => '/path/to/example.gitlab.io'
		'items_per_page' => 20
		// Add anything else you need on the code
	),
	1 => array(
		'type' => 'base',
		'path' => dirname(dirname(__FILE__)) . '/data/html',
		'items_per_page' => 20,
		'prefix' => 'http://example.com/data/'
		// Add anything else you need on the code
	),
);
