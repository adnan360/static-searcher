<?php
namespace StaticSearcher;

class BaseSearcher {
	// What kind of website this class parses
	public $query_type = 'base';
	// Where files to search are
	public $content_directory;
	// The search term input
	public $search_query;
	// Attributes of the file data to search
	public $attributes;
	// File extensions to search in an array
	public $file_types;
	// Simple fuzzy search container variable
	private $sfs;
	// Array containing file related data
	private $files_data;

	// Default settings
	const DEFAULT_ATTRIBUTES = ['file_content'];
	const DEFAULT_FILE_TYPES = ['html', 'md'];

	/**
	 * Constructor for the class.
	 *
	 * @param str $content_directory Where installation/files reside.
	 * @param str $search_query Search term input.
	 * @param array $attributes Attributes to search for.
	 * @param array $file_types Array of file extensions to search for.
	 */
	public function __construct($content_directory, $search_query, $attributes = null, $file_types = null) {
		$this->content_directory = realpath($content_directory);
		$this->search_query = $search_query;
		$this->attributes = (is_array($attributes)) ? $attributes : self::DEFAULT_ATTRIBUTES;
		$this->file_types = (is_array($file_types)) ? $file_types : self::DEFAULT_FILE_TYPES;
	}

	/**
	 * Returns content directory.
	 *
	 * @return str
	 */
	protected function get_content_directory() {
		return $this->content_directory;
	}

	/**
	 * Get a file list filtered by extensions set in settings.
	 * 
	 * @source https://stackoverflow.com/a/3322641
	 * @return array
	 */
	protected function get_file_list() {
		$directory = new \RecursiveDirectoryIterator($this->get_content_directory());
		$flattened = new \RecursiveIteratorIterator($directory);

		// Make sure the path does not contain "/.Trash*" folders and ends with
		// our expected pattern or extension
		return new \RegexIterator(
			$flattened,
			'#^(?:[A-Z]:)?(?:/(?!\.Trash)[^/]+)+/[^/]+\.(?:' . implode( '|', $this->file_types ) . ')$#Di'
		);
	}

	/**
	 * Supply a data array to be used in search for the file.
	 *
	 * @param $file SplFileInfo File information.
	 * @return array
	 */
	protected function get_file_data_array($file) {
		$contents = file_get_contents($file);
		return array(
			'file_name' => $file->getFilename(),
			'file_relative_path' => str_replace($this->get_content_directory().'/', '', $file->getPathname()),
			'file_extension' => $file->getExtension(),
			'file_content' => $contents
		);
	}

	/**
	 * Get information for all configured file formats.
	 */
	protected function get_file_data() {
		$files = $this->get_file_list();

		$this->files_data = array();
		foreach($files as $file) {
			$this->files_data[] = $this->get_file_data_array($file);
		}
 	}
 	
 	/**
 	 * Allows to modify result array on extended classes.
 	 *
 	 * @param array Result array input.
 	 * @return array
 	 */
 	protected function modify_results($results) {
 		return $results;
 	}
	
	/**
	 * Returns search results depending on parameters set earlier.
	 *  
	 * @return array
	 */
	public function results() {
		$this->get_file_data();
		$this->sfs = new \wataridori\SFS\SimpleFuzzySearch($this->files_data, $this->attributes);
		$results = $this->sfs->search($this->search_query);
		return $this->modify_results(array(
			'info' => array(
				'query' => $this->search_query,
				'query_type' => $this->query_type,
				'entries_count' => count($results)
			),
			'entries' => $results
		));
	}
}
