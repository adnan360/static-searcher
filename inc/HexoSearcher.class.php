<?php
namespace StaticSearcher;

class HexoSearcher extends BaseSearcher {
	// What kind of website this class parses
	public $query_type = 'hexo';

	private $hexo_config;
	private $site_url;

	/**
	 * Constructor for the class.
	 *
	 * @param str $content_directory Where installation/files reside.
	 * @param str $search_query Search term input.
	 * @param array $strict_path Do not get the source path from _config.yml, use $content_directory.
	 * @param array $file_types Array of file extensions to search for.
	 * @param str $site_url Site to generate urls for. Useful for mirrored sites. Can be received through, e.g. $_GET['sitesearch'], and passed to this method.
	 */
	public function __construct($content_directory, $search_query, $strict_path = null, $file_types = null, $site_url = null) {
		if (file_exists($content_directory.'/_config.yml')) {
			$this->hexo_config = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($content_directory.'/_config.yml'));
			//echo '<pre>'.print_r($this->hexo_config, true).'</pre>';
		}
		// Hexo content directory
		if ($strict_path) {
			$content_directory = $content_directory;
		} else {
			$content_directory = $content_directory . '/' . $this->hexo_config['source_dir'];
		}
		$file_types = (is_array($file_types)) ? $file_types : ['md'];
		if ($site_url) {
			$this->site_url = $site_url;
		} else {
			$this->site_url = $this->hexo_config['url'];
		}
		
		parent::__construct($content_directory, $search_query, ['file_title', 'file_content'], $file_types);
	}

	/**
	 * Supply a data array to be used in search for the file.
	 *
	 * @param $file SplFileInfo File information.
	 * @return array
	 */
	protected function get_file_data_array($file) {
		$contents = file_get_contents($file);

		// To get yaml and markdown content
		// Index 1 contains the yaml front-matter, index 2 contains the markdown content
		$parts = @preg_split('/[\n]*[-]{3}[\n]/', $contents, 3);
		$yaml = \Symfony\Component\Yaml\Yaml::parse($parts[1]);

		// Permalink generation
		// TODO: implement other variables from https://hexo.io/docs/permalinks
		$permalink_syntax = $this->hexo_config['permalink'];
		$permalink = str_replace(':id', $yaml['id'], $permalink_syntax);
		$permalink = str_replace(':year', date('Y', $yaml['date']), $permalink);
		$permalink = str_replace(':month', date('m', $yaml['date']), $permalink);
		$permalink = str_replace(':day', date('d', $yaml['date']), $permalink);
		$permalink = str_replace(':title', $file->getBasename('.'.$file->getExtension()), $permalink);
		$url = $this->site_url . '/' . $permalink;

		return array(
			'file_name' => $file->getFilename(),
			'file_relative_path' => str_replace($this->get_content_directory().'/', '', $file->getPathname()),
			'file_extension' => $file->getExtension(),
			'file_url' => $url,
			'site_url' => $this->site_url,
			'front_matter' => $yaml,
			'file_title' => @$yaml['title'],
			'file_content' => substr($parts[2], 0, 300)
		);
	}
}
