# StaticSearcher

_- PHP powered fuzzy search engine for static sites -_

Suitable for static sites hosted on basic server or GitHub/GitLab pages where server side functionality is unavailable. This script can be setup on a server which has PHP support. This can successfully generate the URLs for your static site (according to config) and point users to the right page they are looking for.

It requires to have a copy of the website somewhere on the same server as these files. You can use cron+git or [php-gitsync](https://gitlab.com/adnan360/php-gitsync) to keep the source copy updated with the repo to enjoy zero maintenance.

Can serve search service for multiple sites with the same instance through config.


## Install

```bash
git clone --depth=1 https://gitlab.com/adnan360/static-searcher search
cd search
composer install
composer dumpautoload -o
```


### Base

This is ideal to search in any static HTML codebase.

```
cp index_base.sample.php index.php
cp inc/config_sample.inc.php inc/config.inc.php
```

Then edit `inc/config.inc.php`. Make sure to point to the path where you kept your HTML files. Also change the `prefix` value to how you access those files in the browser. Take a note of the config index you are using. Then edit `index.php` and edit the `$config_index` value to be the config index you just prepared.


### Hexo

This can search Hexo website sources and generate proper URLs to point to actual site and the page.

```
cp index_hexo.sample.php index.php
cp inc/config_sample.inc.php inc/config.inc.php
```

Then edit `inc/config.inc.php`. Make sure to point to the path where you kept your Hexo website files. Take a note of the config index you are using. Then edit `index.php` and edit the `$config_index` value to be the config index you just prepared.

Now visit `http://localhost/search/index.php?q=test` or adopt according to where you put these files.

Keep a copy of your static website somewhere and use the appropriate class.
e.g. `\StaticSearcher\HexoSearcher` class to search in a Hexo site.

### Notes

- `config_sample.inc.php` has reference to `data` directory. This is not a strict requirement. You can keep your files anywhere on the server.
- If you want to version track everything, review `.gitignore` to see if everything is as you want it.


## Customize

To modify HTML theme:

```
cp -r templates/default_sample templates/default
```

Then make changes inside `templates/default`. If you want to version control this directory, make sure to remove the directory from `.gitignore`.


## License

MIT (Expat) License
