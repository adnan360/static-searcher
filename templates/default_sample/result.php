<div class="searcher">

<?php if (isset($results['info']['entries_count']) && intval($results['info']['entries_count']) > 0) : ?>

<?php
$query_type = $results['info']['query_type'];
$query = @$_GET['q'];

// Pagination related
$items_per_page = (is_int($config[$config_index]['items_per_page'])) ? $config[$config_index]['items_per_page'] : 20;
$total_page = ceil( $results['info']['entries_count'] / $items_per_page );
$page = intval(@$_GET['page']);
$page = ($page < 2) ? 1 : $page;
$page = ($page > $total_page) ? $total_page : $page;
?>

<p class="info">Search for &quot;<strong><?php echo $results['info']['query']; ?></strong>&quot; returned <strong><?php echo $results['info']['entries_count']; ?></strong> results.</p>
	<div class="entries">
	<?php
	$start_index = ($page - 1) * $items_per_page;
	$end_index = $start_index + $items_per_page;
	$end_index = ( $end_index + 1 > $results['info']['entries_count'] ) ? $results['info']['entries_count'] - 1 : $end_index ;
	?>
	<?php for ( $i = $start_index; $i < $end_index; $i++ ) : ?>
		<div class="entry">
			<?php if ($query_type == 'base'): ?>
			<h4 class="entry-title"><a href="<?php echo $config[$config_index]['prefix'] . $results['entries'][$i][0]['file_relative_path']; ?>"><?php echo $results['entries'][$i][0]['file_name']; ?></a></h4>
			<p class="entry-excerpt"><?php echo $results['entries'][$i][0]['file_content']; ?></p>
			<?php elseif ($query_type == 'hexo'): ?>
			<h4 class="entry-title"><a href="<?php echo $results['entries'][$i][0]['file_url']; ?>"><?php echo $results['entries'][$i][0]['front_matter']['title']; ?></a></h4>
			<p class="entry-excerpt"><?php echo $results['entries'][$i][0]['file_content']; ?></p>
			<?php endif; ?>
		</div>
	<?php endfor; ?>
	</div>
	<div class="pagination">
		<?php
		// Pagination links related
		$url_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
		$url = $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['HTTP_HOST']
				. explode('?', $_SERVER['REQUEST_URI'], 2)[0];
		$prev_link = ( $page == 1 ) ? '' : $url . '?q=' . $query . '&page=' . ($page - 1);
		$next_link = ( $page == $total_page ) ? '' : $url . '?q=' . $query . '&page=' . ($page + 1);
		?>
		<?php if ( $prev_link ) : ?>
		<a href="<?php echo $prev_link; ?>">&laquo; Previous</a>
		<?php endif; ?>
		<?php for ( $j = 1; $j <= $total_page; $j++ ) : ?>
		<a href="<?php echo $url . '?q=' . $query . '&page=' . $j; ?>" <?php if ( $j == $page ) echo 'class="current"'; ?>><?php echo $j; ?></a>
		<?php endfor; ?>
		<?php if ( $next_link ) : ?>
		<a href="<?php echo $next_link; ?>">Next &raquo;</a>
		<?php endif; ?>
	</div>

<?php else: ?>

	<p class="message error">No results returned.</p>

<?php endif; ?>

</div>
