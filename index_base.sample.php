<?php
require_once('vendor/autoload.php');
if (file_exists('inc/config.inc.php')) require_once('inc/config.inc.php'); else die('Error: config.inc.php is not found');
$config_index = 1;

// Use template path which is available
if (realpath('templates/default') == false) {
	$template_path = 'templates/default_sample';
} else {
	$template_path = 'templates/default';
}

$query = @$_GET['q'];

$search = new \StaticSearcher\BaseSearcher($config[$config_index]['path'], $query);
$results = $search->results();

include($template_path . '/header.php');

include($template_path . '/result.php');

include($template_path . '/footer.php');
